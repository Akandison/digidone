import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:gwc/Model/Meter.dart';
import 'package:gwc/Model/MyDropDown.dart';
import 'package:gwc/Model/Objects.dart';
import 'package:gwc/Model/urls.dart';
import 'package:gwc/Pages/my_custom_widgets.dart';
import 'package:gwc/helpers/my_colors.dart';
import 'package:http/http.dart' as http;
import 'package:paystack_manager/models/transaction.dart';
import 'package:paystack_manager/paystack_pay_manager.dart';

class Payment {
  var meterId;
  var amount;
  var reference;
  var phoneNumber;
  int Myamount;
  var token;

  List<String> meterNameList = [];
  List<String> meterIdList = [];
  String firstName;
  final BuildContext context;
  Payment({this.context});
  var storage = FlutterSecureStorage();
  var data;
  bool canSetMeter = false;
  MyObjects objects = Get.find();

  // setMeterId(meter) {
  //   print("From methid");
  //   print(meterId);
  //   this.myMeterId = meter;
  // }

  void checkPayment(email, firstName, lastName, customerId, amount) {
    //int myAmount = int.parse(amount);
    // myAmount = myAmount * 100;
    //0551234987 paystack number
    print("meterId in paymrnt");
    print(meterId);

    try {
      PaystackPayManager(context: context)
        ..setSecretKey("sk_test_cb859b103a9e82864c7dd83893960d234c144286")
        ..setCompanyAssetImage(
            Image(image: AssetImage('images/icons8_plumbing_48px.png')))
        ..setAmount(amount)
        ..setReference(DateTime.now().millisecondsSinceEpoch.toString())
        ..setCurrency("GHS")
        ..setEmail(email)
        ..setFirstName(firstName)
        ..setLastName(lastName)
        ..setMetadata({
          "custom_fields": [
            {
              "meter_id": "$meterId",
              "customer_id": "$customerId",
            }
          ]
        })
        ..onSuccesful(_onPaymentSuccessful)
        ..onPending(_onPaymentPending)
        ..onFailed(_onPaymentFailed)
        ..onCancel(_onCancel)
        ..initialize();
    } catch (error) {
      print("Payment error --- $error");
    }
  }

  // void _onPaymentSuccessful(Transaction transaction) {
  //   print(transaction);
  //   print('Success');
  //   print(
  //       "transaaction message --> ${transaction.message}, Ref ${transaction.refrenceNumber}");
  // }
  void _onPaymentSuccessful(Transaction transaction) async {
    print('Success');
    // print(resp.state);
    var paymntUrl = "$baseUrl/digi_rest/api/payment_handler.php";
    var json = {
      "amount_paid": "$amount",
      "meter_id": "$meterId",
      "reference": "$reference",
      "update_payment": "1",
      "paid_status": "Paid",
      "phone_no": "",
      "payment_method": "Momo/Card",
    };

    var finalPaymentResponse = await http.post(
      paymntUrl,
      body: jsonEncode(json),
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
    );
    var me = jsonDecode(finalPaymentResponse.body);
    print("I am Json");
    print(json);
    print("I am final");
    print(me);
    if (me["message"] == "Success") {
      final snackBar = SnackBar(content: Text('Payment Successful'));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      // Navigator.pop(context);
    }
  }

  void _onPaymentPending(Transaction transaction) {
    final snackBar = SnackBar(content: Text('Payment Pending'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    //Navigator.pop(context);
  }

  void _onPaymentFailed(Transaction transaction) {
    final snackBar = SnackBar(content: Text('Payment Failed'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    //Navigator.pop(context);
  }

  void _onCancel(Transaction transaction) {
    final snackBar = SnackBar(content: Text('Payment Cancelled'));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    //Navigator.pop(context);
  }

  GlobalKey<FormState> _key = GlobalKey<FormState>();
  showMyDialog() {
    var amountController = TextEditingController();
    return Dialog(
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
            Material(
              color: MyColors.primary,
              child: Container(
                height: 50,
                child: Row(
                  children: <Widget>[
                    IconButton(
                      icon: const Icon(
                        Icons.close,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    Text("Enter Amount",
                        style: TextStyle(color: Colors.white, fontSize: 20)),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: Form(
                key: _key,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: [
                        Text("Select meter",
                            style: TextStyle(color: Colors.deepPurple)),
                        Spacer(),
                        MyDropDown(
                          meterNameList: meterNameList,
                          meterIdList: meterIdList,
                          noMeterChanged: (String value) {
                            meterId = value;
                          },
                          onMeterChanged: (String value) {
                            meterId = value;
                          },
                        ),
                      ],
                    ),
                    Container(height: 10),
                    TextFormField(
                      keyboardType: TextInputType.number,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value) {
                        print(value);
                        if (value == "" || value == null) {
                          print("I am here");

                          return "Please enter amount to purchase";
                        }
                        return null;
                      },
                      controller: amountController,
                      style: TextStyle(fontSize: 18),
                      decoration: InputDecoration(
                        hintText:
                            "enter amount to purchase", //"${widget.change}",
                        hintStyle: TextStyle(color: Colors.grey),
                      ),
                    ),
                    Container(height: 10),
                    Align(
                      alignment: Alignment.centerRight,
                      child: RaisedButton(
                          child: Text(
                            "Proceed",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Colors.blue[700],
                          onPressed: () async {
                            //Navigator.pop(context);
                            if (_key.currentState.validate()) {
                              showLoading(context);
                              //Check if token is expired
                              var user_email =
                                  await storage.read(key: "user_email");
                              String id =
                                  await storage.read(key: "customer_id");
                              firstName = await storage.read(key: "first_name");
                              String lastName =
                                  await storage.read(key: "last_name");
                              amount = amountController.text;
                              if (amount != null || amount != "") {
                                token = await storage.read(key: "jwt");
                                var url = Uri.parse(
                                    "$baseUrl/digi_rest/api/handler.php?customer_id=$id&alias=1&no=0");
                                var response = await http.get(
                                  url,
                                  headers: {
                                    HttpHeaders.authorizationHeader:
                                        "Bearer $token"
                                  },
                                );
                                var me = jsonDecode(response.body);
                                if (me["data"] == "Expired token") {
                                  objects.expiredTokenChecks(context);
                                }

                                reference = objects.getRandom();
                                print("meter Id in bf");
                                print(meterId);
                                var paymntUrl =
                                    "$baseUrl/digi_rest/api/payment_handler.php";
                                // meterId = me["data"][0]["meter_id"];
                                var json = {
                                  "user_email": "$user_email",
                                  "customer_id": "$id",
                                  "amount": "$amount",
                                  "meter_id": "$meterId",
                                  "reference": "$reference",
                                  "pay": "1"
                                };

                                var paymentResonse = await http.post(
                                  paymntUrl,
                                  body: jsonEncode(json),
                                  headers: {
                                    HttpHeaders.authorizationHeader:
                                        "Bearer $token"
                                  },
                                );
                                var decoded = jsonDecode(paymentResonse.body);
                                print("Decoded");
                                print(decoded);
                                if (decoded["message"] == "Success") {
                                  Navigator.pop(context);
                                  Navigator.pop(context);
                                  double payment;
                                  try {
                                    payment = double.parse(amount);
                                    payment = payment * 100;
                                    Myamount = payment.ceil();
                                    print("payment double $Myamount");

                                    checkPayment(user_email, firstName,
                                        lastName, id, Myamount);
                                  } catch (FormatException) {
                                    objects.customErrorAlert(
                                        context: context,
                                        title: "Error",
                                        content: "Please enter correct amount");
                                  }
                                } else {
                                  objects.errorAlert(context);
                                }
                              }
                            }
                          }),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

  showSpecificMeterDialog(myMeterId) {
    var amountController = TextEditingController();
    meterId = myMeterId;
    return Dialog(
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Form(
          key: _key,
          child: Column(
            children: <Widget>[
              Material(
                color: MyColors.primary,
                child: Container(
                  height: 50,
                  child: Row(
                    children: <Widget>[
                      IconButton(
                        icon: const Icon(
                          Icons.close,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      Text("Enter Amount",
                          style: TextStyle(color: Colors.white, fontSize: 20)),
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextFormField(
                      keyboardType: TextInputType.number,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value) {
                        print(value);
                        if (value == "" || value == null) {
                          print("I am here");

                          return "Please enter amount to purchase";
                        }
                        return null;
                      },
                      controller: amountController,
                      style: TextStyle(fontSize: 18),
                      decoration: InputDecoration(
                        hintText:
                            "enter amount to purchase", //"${widget.change}",
                        hintStyle: TextStyle(color: Colors.grey),
                      ),
                    ),
                    Container(height: 10),
                    Align(
                      alignment: Alignment.centerRight,
                      child: RaisedButton(
                          child: Text(
                            "Proceed",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Colors.blue[700],
                          onPressed: () async {
                            //Navigator.pop(context);
                            if (_key.currentState.validate()) {
                              showLoading(context);
                              //Check if token is expired
                              var user_email =
                                  await storage.read(key: "user_email");
                              String id =
                                  await storage.read(key: "customer_id");
                              firstName = await storage.read(key: "first_name");
                              String lastName =
                                  await storage.read(key: "last_name");
                              amount = amountController.text;
                              if (amount != null || amount != "") {
                                token = await storage.read(key: "jwt");
                                var url = Uri.parse(
                                    "$baseUrl/digi_rest/api/handler.php?customer_id=$id&alias=1&no=0");
                                var response = await http.get(
                                  url,
                                  headers: {
                                    HttpHeaders.authorizationHeader:
                                        "Bearer $token"
                                  },
                                );
                                var me = jsonDecode(response.body);
                                if (me["data"] == "Expired token") {
                                  objects.expiredTokenChecks(context);
                                }

                                reference = objects.getRandom();
                                print("meter Id in bf");
                                print(meterId);
                                var paymntUrl =
                                    "$baseUrl/digi_rest/api/payment_handler.php";
                                // meterId = me["data"][0]["meter_id"];
                                var json = {
                                  "user_email": "$user_email",
                                  "customer_id": "$id",
                                  "amount": "$amount",
                                  "meter_id": "$meterId",
                                  "reference": "$reference",
                                  "pay": "1"
                                };

                                var paymentResonse = await http.post(
                                  paymntUrl,
                                  body: jsonEncode(json),
                                  headers: {
                                    HttpHeaders.authorizationHeader:
                                        "Bearer $token"
                                  },
                                );
                                var decoded = jsonDecode(paymentResonse.body);
                                print("Decoded");
                                print(decoded);
                                if (decoded["message"] == "Success") {
                                  Navigator.pop(context);
                                  Navigator.pop(context);
                                  double payment;
                                  try {
                                    payment = double.parse(amount);
                                    payment = payment * 100;
                                    Myamount = payment.ceil();
                                    print("payment double $Myamount");

                                    checkPayment(user_email, firstName,
                                        lastName, id, Myamount);
                                  } catch (FormatException) {
                                    objects.customErrorAlert(
                                        context: context,
                                        title: "Error",
                                        content: "Please enter correct amount");
                                  }
                                } else {
                                  objects.errorAlert(context);
                                }
                              }
                            }
                          }),
                    ),
                  ],
                ),
              ),
              Container(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void meterIdAndName(context) {
    if (data != null) {
      meterNameList.clear();
      meterIdList.clear();
      for (int i = 0; i < data.length; i++) {
        meterNameList.insert(i, data[i]["meter_alias"]);
        meterIdList.insert(i, data[i]["meter_id"]);
      }
      print("meter List");
      print(meterNameList);
      print(meterIdList);
    }
    //This code doesn't do anything
    else {
      Navigator.pop(context);
      objects.customErrorAlert(
        context: context,
        title: "Error",
        content: "No meter added",
      );
    }
  }

  getMeterInfo({@required BuildContext context}) async {
    showLoading(context);
    String id = await storage.read(key: "customer_id");
    String token = await storage.read(key: "jwt");

    var url = Uri.parse(
        "$baseUrl/digi_rest/api/handler.php?customer_id=$id&alias=1&no=0");
    await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
    ).then((response) {
      print(response.body);
      var me = jsonDecode(response.body);
      print("Expired token checks");
      print(me["data"]);
      //currentLock = me["data"][0]["lock_status"];
      if (me["data"] == "Expired token") {
        objects.expiredTokenChecks(context);
      }
      if (me["data"] != null) {
        data = me["data"];

        print("my data");
        print(data);
        meterIdAndName(context);
        Navigator.pop(context);
      }
    });
  }
}
