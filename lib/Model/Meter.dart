import 'package:flutter/material.dart';

class MeterObject {
  final meterId;
  final meterName;
  MeterObject({@required this.meterId, @required this.meterName});
}
