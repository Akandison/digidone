import 'package:flutter/material.dart';
import 'package:flutter_dropdown/flutter_dropdown.dart';
import 'package:gwc/Model/Meter.dart';
import 'package:gwc/Model/paymentFile.dart';
import 'package:gwc/Pages/MeterView.dart';
import 'package:http/http.dart' as http;
import 'package:syncfusion_flutter_charts/charts.dart';

class MyDropDown extends StatefulWidget {
  @override
  _MyDropDownState createState() => _MyDropDownState();
  final List<String> meterNameList;
  final List<String> meterIdList;
  final Function(String) onMeterChanged;
  final Function(String) noMeterChanged;
  var meterId;
  MyDropDown(
      {@required this.meterNameList,
      @required this.meterIdList,
      @required this.onMeterChanged,
      this.noMeterChanged});
}

class _MyDropDownState extends State<MyDropDown> {
  String dropdownValue;
  Payment payment = Payment();

  @override
  void initState() {
    super.initState();
    dropdownValue = widget.meterNameList.first;
    widget.noMeterChanged(getFirstMeterId());
  }

//this function returns the meterId of first meter on the list when user has not selected any meter
  String getFirstMeterId() {
    return widget.meterIdList[widget.meterNameList.indexOf(dropdownValue)];
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownValue,
      icon: const Icon(Icons.arrow_downward),
      iconSize: 16,
      elevation: 16,
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onTap: () {
        var selectedIndex = widget.meterNameList.indexOf(dropdownValue);
        widget.onMeterChanged(widget.meterIdList[selectedIndex]);
      },
      onChanged: (String newValue) {
        setState(() {
          if (newValue != null) {
            dropdownValue = newValue;
            print("Selected value");
            print(dropdownValue);
            var selectedIndex = widget.meterNameList.indexOf(dropdownValue);
            // print("Selected meter");
            // payment.meterId = widget.meterIdList[selectedIndex];
            // widget.meterId = widget.meterIdList[selectedIndex];
            // payment.setMeterId(widget.meterIdList[selectedIndex]);

            widget.onMeterChanged(widget.meterIdList[selectedIndex]);
          }
        });
      },
      items: widget.meterNameList.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
