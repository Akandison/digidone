import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:gwc/Model/cutom_dialog.dart';
import 'package:gwc/Model/paymentFile.dart';
import 'package:gwc/Pages/DashBoard.dart';
import 'package:gwc/Pages/LogIn.dart';
import 'package:gwc/Pages/my_custom_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:paystack_manager/models/api_response.dart';
import 'package:paystack_manager/paystack_pay_manager.dart';
import 'package:toast/toast.dart';
import 'urls.dart';
import 'MeterClass.dart';
import 'package:bottom_sheet/bottom_sheet.dart';
import 'package:gwc/helpers/my_textStyles.dart';
import 'package:gwc/helpers/my_colors.dart';
import 'package:get/get.dart';

class MyObjects extends GetxController {
  var meterId;
  var amount;
  var reference;
  var phoneNumber;
  int Myamount;
  var token;
  String firstName;
  BuildContext myContext;
  var meters = List<Meter>().obs;
  bool isAdding;
  Urls objects = Urls();
  var meterObj = new Meter();
  // var meterCont = Get.put(Meter(), permanent: true).obs;
  final storage = FlutterSecureStorage();
  var myBalance;
  var meterNameList;
  var meterIdList;
  var data;

  List<TextInputFormatter> phoneNumberFormatter() {
    return [
      new FilteringTextInputFormatter.allow(new RegExp(r'^[0-9]*$')),
      new LengthLimitingTextInputFormatter(10)
    ];
  }

  RegExp emailRegex() {
    final RegExp emailRegex = new RegExp(
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
    return emailRegex;
  }

  RegExp phoneregex() {
    final RegExp phoneRegex = new RegExp(r'^[6-9]\d{9}$');
    return phoneRegex;
  }

  Future<String> attemptLogIn(String username, String password) async {
    final myheaders = {
      "Content-type": "application/json",
      "accept": "application/json"
    };
    final json = {"user_email": "$username", "password": "$password"};

    var res = await http.post(
      Uri.parse(objects.signInUrl),
      body: jsonEncode(json),
      headers: myheaders,
    );
    print("Its working");
    print(res.body);
    if (res.statusCode == 200) {
      print("It worked");
      print(res.body);
      return res.body;
    }
    return null;
  }

  void displayDialog(context, title, text) => showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(title: Text(title), content: Text(text)),
      );

  void addEditMeter(
      {String meterName,
      String meterId,
      String uniqueName,
      String meterAddr,
      int index}) {
    meterObj.meterName = meterName;
    meterObj.meterId = meterId;
    meterObj.uniqueName = uniqueName;
    meterObj.meterAddr = meterAddr;

    if (isAdding)
      meters.add(meterObj);
    else
      meters[index] = meterObj;
  }

  void removeMeter(index) {
    meters.removeAt(index);
  }

  String getRandom() {
    final r = Random();
    return List<int>.generate(12, (index) => r.nextInt(10))
        .fold<String>("", (prev, i) => prev += i.toString());
  }

  void loadData() {
    Meter meterObj0 = new Meter(
      meterName: "iMan",
      meterId: "0114528455",
      uniqueName: "Ogwangwalu",
      balance: "-20",
      meterState: "OFF",
      meterAddr: "BU-1124-8194",
    );

    Meter meterObj1 = new Meter(
        meterName: "Kwadwo",
        meterId: "0114855",
        uniqueName: "Kwadee",
        balance: "78",
        meterState: "ON",
        meterAddr: "584d-BU");
    Meter meterObj2 = new Meter(
      meterName: "Trek",
      meterId: "011425",
      uniqueName: "Ammobra",
      balance: "15",
      meterState: "ON",
      meterAddr: "584dad-BU",
    );
    Meter meterObj3 = new Meter(
        meterName: "Ojoe",
        meterId: "01145138",
        uniqueName: "Jake",
        balance: "700",
        meterState: "ON",
        meterAddr: "5ghd-BU");
    meters.add(meterObj0);
    meters.add(meterObj1);
    meters.add(meterObj2);
    meters.add(meterObj3);
  }

  expiredTokenChecks(BuildContext context) async {
    Navigator.pop(context);
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Expired Session"),
          content: Text("You will be directed to login"),
          actions: <Widget>[
            FlatButton(
              child: const Text('OKAY'),
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => LogIn(),
                  ),
                  (route) => false,
                );
              },
            )
          ],
        );
      },
    );
  }

  errorAlert(BuildContext context) {
    Navigator.pop(context);
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Error"),
          content: Text("Unable to finish task, try again in a moment"),
          actions: <Widget>[
            FlatButton(
              child: const Text('OKAY'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  logOut(BuildContext context, FlutterSecureStorage storage) async {
    showLoading(context);
    await storage.deleteAll();
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) => LogIn(),
      ),
      (route) => false,
    );
  }

  customErrorAlert(
      {@required BuildContext context, @required title, @required content}) {
    Navigator.pop(context);
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: const Text('OKAY'),
              onPressed: () {
                // Navigator.of(context).pop();
                Navigator.pop(context);
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }

  dashBoardNavDialog({BuildContext context, firstName, text, content}) {
    Navigator.pop(context);
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(text),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: const Text('OKAY'),
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => DashBoard(firstName),
                  ),
                  (route) => false,
                );
              },
            )
          ],
        );
      },
    );
  }

  getBalance(balance) {
    myBalance = balance;
    return myBalance;
  }

  // void meterIdAndName() {
  //   if (meterNameList != null && meterIdList != null) {
  //     meterNameList.clear();
  //     meterIdList.clear();
  //     for (int i = 0; i < data.length; i++) {
  //       meterNameList.insert(i, data[i]["meter_alias"]);
  //       meterIdList.insert(i, data[i]["meter_id"]);
  //     }
  //     print("meter List");
  //     print(meterNameList);
  //     print(meterIdList);
  //   }
  // }
  //
  // getMeterInfo({@required BuildContext context}) async {
  //   showLoading(context);
  //   String id = await storage.read(key: "customer_id");
  //   String token = await storage.read(key: "jwt");
  //
  //   var url = Uri.parse(
  //       "$baseUrl/digi_rest/api/handler.php?customer_id=$id&alias=1&no=0");
  //   await http.get(
  //     url,
  //     headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
  //   ).then((response) {
  //     print(response.body);
  //     var me = jsonDecode(response.body);
  //     print("Expired token checks");
  //     print(me["data"]);
  //     //currentLock = me["data"][0]["lock_status"];
  //     if (me["data"] == "Expired token") {
  //       expiredTokenChecks(context);
  //     }
  //     if (me["data"] != null) {
  //       data = me["data"];
  //       print("my data");
  //       print(data);
  //       meterIdAndName();
  //       payment.meterIdList = meterIdList;
  //       payment.meterNameList = meterNameList;
  //     }
  //   });
  // }
}
