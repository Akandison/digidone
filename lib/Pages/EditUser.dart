import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:gwc/Model/Objects.dart';
import 'package:gwc/Model/urls.dart';
import 'package:gwc/Pages/DashBoard.dart';
import 'package:gwc/Pages/LogIn.dart';
import 'package:gwc/Pages/my_custom_widgets.dart';
import 'package:gwc/helpers/my_colors.dart';
import 'package:gwc/helpers/my_textStyles.dart';
import 'package:http/http.dart' as http;

class EditUser extends StatefulWidget {
  final Map userData;
  EditUser(this.userData);

  @override
  EditUserState createState() => new EditUserState();
}

class EditUserState extends State<EditUser> {
  // List<String> userData = [];
  final storage = FlutterSecureStorage();
  TextEditingController firstNameController;
  TextEditingController lastNameController;
  TextEditingController emailController;
  TextEditingController numberController;
  TextEditingController cityController;
  TextEditingController streetController;
  TextEditingController regionController;
  TextEditingController digitalAddressController;

  MyObjects objects = Get.find();

  var emailRegex;
  bool validEmail = false;
  bool validFirstName = false;
  bool validLastName = false;
  bool validNumber = false;
  bool validDigitalAddress = false;
  bool validStreetAddress = false;
  bool validCity = false;
  bool validRegion = false;

  var phoneRegex;
  var first_name;
  var last_name;
  var user_email;
  var phone_no;
  var address_city;
  var address_street;
  var address_region;
  var digital_address;

  //This method gives initial values to textfield
  void initialDetails() {
    firstNameController =
        new TextEditingController(text: widget.userData["first_name"]);
    lastNameController =
        new TextEditingController(text: widget.userData["last_name"]);
    emailController =
        new TextEditingController(text: widget.userData["user_email"]);
    numberController =
        new TextEditingController(text: widget.userData["phone_no"]);
    cityController =
        new TextEditingController(text: widget.userData["address_city"]);
    streetController =
        new TextEditingController(text: widget.userData["address_street"]);
    regionController =
        new TextEditingController(text: widget.userData["address_region"]);
    digitalAddressController =
        new TextEditingController(text: widget.userData["digital_address"]);
    firstNameController.text = widget.userData["first_name"];
    lastNameController.text = widget.userData["last_name"];
    emailController.text = widget.userData["user_email"];
    numberController.text = widget.userData["phone_no"];
    cityController.text = widget.userData["address_city"];
    streetController.text = widget.userData["address_street"];
    regionController.text = widget.userData["address_region"];
    digitalAddressController.text = widget.userData["digital_address"];
  }

  editUserData() async {
    showLoading(context);

    String token = await storage.read(key: "jwt");
    var myUrl = "$baseUrl/digi_rest/api/edit_profile_service.php";
    final json = {
      "user_email": "$user_email",
      "last_name": "$last_name",
      "first_name": "$first_name",
      "phone_no": "$phone_no",
      "digital_address": "$digital_address",
      "address_city": "$address_city",
      "address_region": "$address_region",
      "address_street": "$address_street",
      "no_password": "1"
    };
    print(json);
    print(user_email);
    final response = await http.post(Uri.parse(myUrl),
        headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
        body: jsonEncode(json));
    print('Status code: ${response.statusCode}');
    //  print('Body: ${response.body}');
    var me = jsonDecode(response.body);
    print("Response from edit");
    print(me);
    if (me["message"] == "Success") {
      await storage.write(key: "user_email", value: user_email);
      await storage.write(key: "phone_no", value: phone_no);
      await storage.write(key: "first_name", value: first_name);
      await storage.write(key: "last_name", value: last_name);
      await storage.write(key: "digital_address", value: digital_address);
      await storage.write(key: "address_street", value: address_street);
      await storage.write(key: "address_city", value: address_city);
      await storage.write(key: "address_region", value: address_region);
      var firstName = await storage.read(key: "first_name");
      Navigator.pop(context);
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Success"),
            content: Text("You will be redirected to your dashboard"),
            actions: <Widget>[
              FlatButton(
                child: const Text('OKAY'),
                onPressed: () {
                  Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => DashBoard(firstName),
                    ),
                    (route) => false,
                  );
                },
              )
            ],
          );
        },
      );
    }
    if (me["data"] == "Expired token") {
      objects.expiredTokenChecks(context);
    }
  }

  validate() async {
    print("Hi");

    print("email");
    print(validEmail);
    print("first");
    print(validFirstName);
    // if (validStreetAddress == true &&
    //     validRegion == true &&
    //     validNumber == true &&
    //     validLastName == true &&
    //     validFirstName == true &&
    //     validCity == true &&
    //     validEmail == true &&
    //     validDigitalAddress == true)
    if (first_name != "" &&
        last_name != "" &&
        user_email != "" &&
        phone_no != "" &&
        digital_address != "" &&
        address_street != "" &&
        address_city != "" &&
        address_region != "") {
      print("all ready");
      await editUserData();
    }
  }

  @override
  void initState() {
    super.initState();
    initialDetails();
  }

  @override
  Widget build(BuildContext context) {
    emailRegex = objects.emailRegex;
    phoneRegex = objects.phoneregex();
    return Scaffold(
      backgroundColor: MyColors.grey_5,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.blue),
        elevation: 1,
        backgroundColor: Colors.white,
        title: Text("Edit Profile",
            style: MyText.title(context).copyWith(
              color: Colors.blue,
            )),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        scrollDirection: Axis.vertical,
        child: Align(
          alignment: Alignment.topCenter,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("FIRST NAME",
                    style: MyText.body1(context)
                        .copyWith(color: MyColors.grey_60)),
                Container(height: 5),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3),
                  ),
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  margin: EdgeInsets.all(0),
                  elevation: 0,
                  child: Container(
                    height: 40,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: TextFormField(
                      validator: (value) {
                        value = firstNameController.text;
                        if (value == "" || value == null) {
                          validFirstName = false;
                          return 'Please enter your first name';
                        }
                        validFirstName = true;
                        return null;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      maxLines: 1,
                      controller: firstNameController,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(-12),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
                Container(height: 15),
                Text("LAST NAME",
                    style: MyText.body1(context)
                        .copyWith(color: MyColors.grey_60)),
                Container(height: 5),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3),
                  ),
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  margin: EdgeInsets.all(0),
                  elevation: 0,
                  child: Container(
                    height: 40,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: TextFormField(
                      validator: (value) {
                        if (value == "" || value == null) {
                          validLastName = false;
                          return 'Please enter a last name';
                        }
                        validLastName = true;
                        return null;
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      maxLines: 1,
                      controller: lastNameController,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(-12),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
                Container(height: 15),
                Text("EMAIL",
                    style: MyText.body1(context)
                        .copyWith(color: MyColors.grey_60)),
                Container(height: 5),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3),
                  ),
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  margin: EdgeInsets.all(0),
                  elevation: 0,
                  child: Container(
                    height: 40,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: TextFormField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value) {
                        if (!emailRegex.hasMatch(value)) {
                          validEmail = false;
                          return 'Please enter a valid email';
                        }
                        if (value == '' || value == null) {
                          validEmail = false;
                          return 'Please enter a valid email';
                        }
                        validEmail = true;
                        return null;
                      },
                      maxLines: 1,
                      keyboardType: TextInputType.emailAddress,
                      controller: emailController,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(-12),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
                Container(height: 15),
                Text("PHONE",
                    style: MyText.body1(context)
                        .copyWith(color: MyColors.grey_60)),
                Container(height: 5),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3),
                  ),
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  margin: EdgeInsets.all(0),
                  elevation: 0,
                  child: Container(
                    height: 40,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      inputFormatters: objects.phoneNumberFormatter(),
                      validator: (value) {
                        if (phoneRegex.hasMatch(value)) {
                          validNumber = false;
                          return 'Please enter valid phone number';
                        }
                        if (value.length != 10) {
                          validNumber = false;
                          return 'Please enter a valid number';
                        }

                        validNumber = true;
                        return null;
                      },
                      maxLines: 1,
                      controller: numberController,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(-12),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
                Container(height: 15),
                Text("DIGITAL ADDRESS",
                    style: MyText.body1(context)
                        .copyWith(color: MyColors.grey_60)),
                Container(height: 5),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3),
                  ),
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  margin: EdgeInsets.all(0),
                  elevation: 0,
                  child: Container(
                    height: 40,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: TextFormField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value) {
                        if (value == "" || value == null) {
                          validDigitalAddress = false;
                          return 'Please enter your digital address';
                        }
                        validDigitalAddress = true;
                        return null;
                      },
                      maxLines: 1,
                      controller: digitalAddressController,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(-12),
                        border: InputBorder.none,
                      ),
                    ),
                  ),
                ),
                Container(height: 15),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("CITY",
                              style: MyText.body1(context)
                                  .copyWith(color: MyColors.grey_60)),
                          Container(height: 5),
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(3),
                            ),
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            margin: EdgeInsets.all(0),
                            elevation: 0,
                            child: Container(
                              height: 40,
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.symmetric(horizontal: 5),
                              child: Row(
                                children: <Widget>[
                                  Container(width: 15),
                                  Expanded(
                                    child: TextFormField(
                                      autovalidateMode:
                                          AutovalidateMode.onUserInteraction,
                                      validator: (value) {
                                        if (value == "" || value == null) {
                                          validCity = false;
                                          return 'Please enter the name of your city';
                                        }
                                        validCity = true;
                                        return null;
                                      },
                                      maxLines: 1,
                                      controller: cityController,
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.all(-12),
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(width: 15),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("STREET",
                              style: MyText.body1(context)
                                  .copyWith(color: MyColors.grey_60)),
                          Container(height: 5),
                          Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(3),
                            ),
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            margin: EdgeInsets.all(0),
                            elevation: 0,
                            child: Container(
                              height: 40,
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.symmetric(horizontal: 20),
                              child: TextFormField(
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                validator: (value) {
                                  if (value == "" || value == null) {
                                    validStreetAddress = false;
                                    return 'Please enter the name of your street';
                                  }
                                  validStreetAddress = true;
                                  return null;
                                },
                                maxLines: 1,
                                controller: streetController,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(-12),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Container(height: 15),
                Text(
                  "REGION",
                  style:
                      MyText.body1(context).copyWith(color: MyColors.grey_60),
                ),
                Container(height: 5),
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(3),
                  ),
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  margin: EdgeInsets.all(0),
                  elevation: 0,
                  child: Container(
                    height: 40,
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: 5),
                    child: Row(
                      children: <Widget>[
                        Container(width: 15),
                        Expanded(
                          child: TextFormField(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            validator: (value) {
                              print("region value");
                              print(value);
                              if (value == "" || value == null) {
                                validRegion = false;
                                return 'Please enter your region';
                              }
                              validRegion = true;
                              return null;
                            },
                            maxLines: 1,
                            controller: regionController,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(-12),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        // Icon(Icons.expand_more, color: MyColors.grey_40)
                      ],
                    ),
                  ),
                ),
                Container(height: 15),
                Container(
                  width: double.infinity,
                  height: 45,
                  child: FlatButton(
                    child: Text("SUBMIT",
                        style: MyText.subhead(context)
                            .copyWith(color: Colors.white)),
                    color: MyColors.primary,
                    onPressed: () async {
                      first_name = firstNameController.text;
                      last_name = lastNameController.text;
                      user_email = emailController.text;
                      digital_address = digitalAddressController.text;
                      address_street = streetController.text;
                      address_city = cityController.text;
                      address_region = regionController.text;
                      phone_no = numberController.text;
                      await validate();
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
