import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:back_pressed/back_pressed.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:gwc/Model/Objects.dart';
import 'package:gwc/Pages/my_custom_widgets.dart';
import 'package:gwc/helpers/my_colors.dart';
import 'package:gwc/helpers/my_textStyles.dart';
import 'package:http/http.dart' as http;
//import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:gwc/Model/urls.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:intl/intl.dart';
import 'package:animated_radio_buttons/animated_radio_buttons.dart';

class MyGraph extends StatefulWidget {
  final details;
  final readList;
  final timeList;
  final index;
  MyGraph({this.details, this.readList, this.timeList, this.index});
  @override
  _GraphState createState() => _GraphState();
}

class _GraphState extends State<MyGraph> {
  var storage = FlutterSecureStorage();
  var timer;
  var entriesList;
  var reading;
  var isReadingLoading = true;
  var balance;
  var isBalanceLoading = true;
  int responseLength = 10;
  var listReady = false;
  var canSetState = true;
  var balanceEmpty = true;
  final double _min = 0.0;
  final double _max = 15.0;
  List<Meter> plotList = [];
  int timeFormatIndex = 1;
  DateFormat currentDateFormat = DateFormat.Hm();
  MyObjects objectsCont = Get.find();
  @override
  void initState() {
    super.initState();
    allInOne();
    setUpTimedFetch();
  }

  balanceChecker() async {
    String id = await storage.read(key: "customer_id");
    String token = await storage.read(key: "jwt");

    var url = Uri.parse(
        "$baseUrl/digi_rest/api/handler.php?customer_id=$id&alias=1&no=0");
    http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
    ).then((response) {
      var me = jsonDecode(response.body);
      print("working");
      print(me);
      if (me["data"] == "Expired token") {
        timer.cancel();
        objectsCont.expiredTokenChecks(context);
      }
      print("Sinlge meter");
      print(me["data"][widget.index]);
      //only call the set state when the stateful widget has not be disposed to prevent memory leakage
      if (canSetState) {
        setState(() {
          balance = me["data"][widget.index]["meter_account"];
          isBalanceLoading = false;
        });
      }

      // readingChecker();
    });
  }

  getDataMap() {
    // if(widget.timeList!=null && widget.readList!=null){}
    // widget.timeList.clear();
    //widget.readList.clear();
    int listSize = entriesList.length;
    int start = listSize - 11;
    int readTimeIndex = 0;
    for (int i = 0; i < 10; i++) {
      print("where I want");
      //var myReading = entriesList[i]["reading"].toString();
      String myReading = "${entriesList[i]["volume_consumed"]}";
      print(myReading.runtimeType);
      // widget.readList.insert(i, myReading);

      widget.readList.insert(readTimeIndex, myReading);
      String now = entriesList[i]["entry_time"].toString();

      //now=double.parse(source)
      //var myTy = entriesList[i]["entry_time"].toString();
      print("I am now");
      print(now);
      print(now.runtimeType);
      //now=now.substring()
      //widget.timeList.insert(i, now);
      widget.timeList.insert(readTimeIndex, now);
      readTimeIndex++;
    }

    //entriesMap["readings"] = readList;
    //entriesMap["time"] = timeList;
    print("Reading and time");
    print(widget.readList);
    print(widget.timeList);
    listReady = true;
  }

  readingChecker() async {
    // showLoading(context);
    //String id = await storage.read(key: "customer_id");
    String token = await storage.read(key: "jwt");
    var meterId = widget.details["meter_id"];
    //var firstName = await storage.read(key: "first_name");

    var url = Uri.parse(
        "$baseUrl/digi_rest/api/handler2.php?meter_id=$meterId&get_readings=1");
    http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
    ).then((response) async {
      var me = jsonDecode(response.body);

      // print("I am entry");
      // print(entriesList[0]["entry_time"]);
      //setDataList(me);

      if (me["data"] != null) {
        //only make a call to setState when the stateful widget has not been disposed to prevent memory leakage
        if (canSetState) {
          setState(() {
            entriesList = me["data"];

            reading = me["data"][0]["volume_consumed"];
            print(reading);
            isReadingLoading = false;
            balanceEmpty = false;
          });
        }
      }

      getDataMap();
    });
  }

  bool listsReady() {
    if (widget.readList != null && widget.timeList != null) {
      return false;
    } else {
      return true;
    }
  }

  getMapList() {
    print("Reading");
    print(widget.timeList);
    print(widget.readList);
    plotList.clear();
    for (int i = 0; i < 10; i++) {
      // plotList.insert(i, Meter(widget.readList[i], widget.timeList[i]));

      plotList.insert(
        i,
        Meter(
          //widget.readList[i].toString(),
          double.parse(widget.readList[i]),
          DateTime.parse(widget.timeList[i]),
        ),
      );
      // plotList.insert(i, Meter(alo[i].toString(), widget.timeList[i]));
    }
    print("plotLits");
    print(plotList[0].meterReading);
    return plotList;
  }

  setUpTimedFetch() {
    timer = Timer.periodic(Duration(milliseconds: 5000), (timer) {
      balanceChecker();
      readingChecker();
    });
  }

  allInOne() async {
    await readingChecker();
    await balanceChecker();
  }

  @override
  void dispose() {
    canSetState = false;
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final List<Color> color = <Color>[];
    color.add(Colors.deepOrange[50]);
    color.add(Colors.deepPurpleAccent);
    color.add(Colors.deepPurple);

    final List<double> stops = <double>[];
    stops.add(0.0);
    stops.add(0.5);
    stops.add(1.0);

    final LinearGradient gradientColors =
        LinearGradient(colors: color, stops: stops);
    return OnBackPressed(
      perform: () {
        print("Back Pressed");
        timer.cancel();
        Navigator.pop(context);
      },
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          backgroundColor: Colors.white,
          brightness: Brightness.dark,
          iconTheme: IconThemeData(color: Colors.blue),
          title: Text("Consumption Rate V time",
              style: MyText.title(context).copyWith(color: Colors.blue)),
        ),
        body: isBalanceLoading == true &&
                isReadingLoading == true &&
                listsReady() == true
            ? ListView.builder(
                shrinkWrap: true,
                itemCount: 3,
                itemBuilder: (BuildContext context, int index) {
                  return LoadingMeters();
                })
            : Container(
                child: Column(
                  children: [
                    Container(height: 15),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      height: MediaQuery.of(context).size.height * 0.75,
                      width: MediaQuery.of(context).size.width * 1.2,
                      child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Card(
                          child: SfCartesianChart(
                              enableAxisAnimation: true,
                              primaryXAxis: DateTimeAxis(
                                dateFormat: currentDateFormat,
                                edgeLabelPlacement: EdgeLabelPlacement.shift,
                                title: AxisTitle(
                                  text: 'Time',
                                  textStyle: TextStyle(
                                      color: Colors.deepOrange,
                                      fontFamily: 'Roboto',
                                      fontSize: 16,
                                      fontStyle: FontStyle.italic,
                                      fontWeight: FontWeight.w300),
                                ),
                              ),
                              primaryYAxis: NumericAxis(
                                minimum: _min,
                                maximum: _max,
                                title: AxisTitle(
                                  text: 'Volume (mL)',
                                  textStyle: TextStyle(
                                      color: Colors.deepOrange,
                                      fontFamily: 'Roboto',
                                      fontSize: 16,
                                      fontStyle: FontStyle.italic,
                                      fontWeight: FontWeight.w300),
                                ),
                              ),
                              // Chart title
                              title: ChartTitle(
                                  text:
                                      'Current meter reading is: $reading mL'),
                              // Enable legend
                              legend: Legend(isVisible: true),
                              // Enable tooltip
                              tooltipBehavior: TooltipBehavior(enable: true),
                              series: <ChartSeries<Meter, DateTime>>[
                                //<ChartSeries<Meter, DateTime>>[
                                SplineAreaSeries<Meter, DateTime>(
                                    dataSource: getMapList(),
                                    cardinalSplineTension: 0.9,
                                    xValueMapper: (Meter meter, _) =>
                                        meter.time,
                                    yValueMapper: (Meter meter, _) =>
                                        //double.parse(meter.meterReading),
                                        meter.meterReading,
                                    gradient: gradientColors,
                                    borderWidth: 4,
                                    borderGradient: const LinearGradient(
                                        colors: <Color>[
                                          Color.fromRGBO(230, 0, 180, 1),
                                          Color.fromRGBO(255, 200, 0, 1)
                                        ],
                                        stops: <double>[
                                          0.2,
                                          0.9
                                        ]),
                                    emptyPointSettings: EmptyPointSettings(
                                      // Mode of empty point
                                      mode: EmptyPointMode.zero,
                                    ),
                                    name: 'Usage plot',
                                    // Enable data label
                                    dataLabelSettings:
                                        DataLabelSettings(isVisible: true)),
                              ]),
                        ),
                      ),
                    ),
                    Container(
                      height: 30,
                    ),
                    Container(
                      // height: MediaQuery.of(context).size.height * 0.1,
                      width: MediaQuery.of(context).size.width * 1.5,
                      // margin: EdgeInsets.only(top: 44),
                      child: Card(
                        elevation: 3,
                        child: AnimatedRadioButtons(
                          backgroundColor: Colors.white10,
                          value: timeFormatIndex ?? 0,
                          layoutAxis: Axis.horizontal,
                          buttonRadius: 28.0,
                          items: [
                            AnimatedRadioButtonItem(
                                label: "Hr/Min",
                                color: MyColors.primary,
                                fillInColor: Colors.grey),
                            AnimatedRadioButtonItem(
                                label: "Months",
                                color: MyColors.primary,
                                fillInColor: Colors.grey),
                            AnimatedRadioButtonItem(
                                label: "Years",
                                color: MyColors.primary,
                                fillInColor: Colors.grey),
                          ],
                          onChanged: (value) {
                            setState(() {
                              timeFormatIndex = value;
                              print(timeFormatIndex);
                              switch (value) {
                                case 0:
                                  {
                                    currentDateFormat = DateFormat.Hm();
                                  }
                                  break;
                                case 1:
                                  {
                                    currentDateFormat = DateFormat.MMM();
                                  }
                                  break;
                                case 2:
                                  {
                                    currentDateFormat = DateFormat.y();
                                  }
                              }
                            });
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}

class Meter {
  Meter(this.meterReading, this.time);
  final double meterReading;
  final DateTime time;
}

//

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           title: const Text('Consumption Rate'),
//         ),
//         body:
// }
//
