import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gwc/Pages/About.dart';
import 'package:gwc/Pages/EditUser.dart';
import 'package:gwc/Pages/LogIn.dart';
import 'package:gwc/Pages/UserInfo.dart';
import 'package:gwc/Pages/password.dart';
import 'package:gwc/helpers/my_textStyles.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SettingsPage extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final storage = FlutterSecureStorage();
  SettingsPage();
  var userDataMap = Map();
  String firstName = '';
  String lastName = '';
  String userEmail = '';
  String phoneNo = '';
  String addressCity = '';
  String addressStreet = '';
  String addressRegion = '';
  String digitalAddress = '';
  Future setUserInfo() async {
    firstName = await storage.read(key: "first_name");
    userDataMap["first_name"] = firstName;
    lastName = await storage.read(key: "last_name");
    userDataMap["last_name"] = lastName;
    userEmail = await storage.read(key: "user_email");
    userDataMap["user_email"] = userEmail;
    phoneNo = await storage.read(key: "phone_no");
    userDataMap["phone_no"] = phoneNo;
    addressCity = await storage.read(key: "address_city");
    userDataMap["address_city"] = addressCity;
    addressStreet = await storage.read(key: "address_street");
    userDataMap["address_street"] = addressStreet;
    addressRegion = await storage.read(key: "address_region");
    userDataMap["address_region"] = addressRegion;
    digitalAddress = await storage.read(key: "digital_address");
    userDataMap["digital_address"] = digitalAddress;
    return userDataMap;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.blue),
        elevation: 1,
        backgroundColor: Colors.white,
        title: Text("Settings",
            style: MyText.title(context).copyWith(
              color: Colors.blue,
            )),
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.person_pin),
            title: Text('Profile'),
            onTap: () async {
              Map returnedUserData = await setUserInfo();
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => EditUser(returnedUserData)),
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.lock),
            title: Text('Change Password'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Password()),
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.description),
            title: Text('Terms and Privacy Policy'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.info_outline),
            title: Text('App Info'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => AboutAppRoute (),
                ),

              );
            },
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Log Out'),
            onTap: () async {
              await storage.deleteAll();
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => LogIn(),
                ),
                (route) => false,
              );
            },
          ),
        ],
      ),
    );
  }
}
