import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:gwc/Model/Objects.dart';
import 'package:gwc/helpers/images.dart';
import 'package:gwc/helpers/my_colors.dart';
import 'package:gwc/helpers/my_textStyles.dart';

class AboutAppRoute extends StatefulWidget {
  AboutAppRoute();

  @override
  AboutAppRouteState createState() => new AboutAppRouteState();
}

class AboutAppRouteState extends State<AboutAppRoute> {
  MyObjects objects = Get.find();
  final storage = FlutterSecureStorage();
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: MyColors.grey_10,
      appBar: AppBar(
        backgroundColor: MyColors.primary,
        title: Text("About"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: (String value) async {
              await objects.logOut(context, storage);
            },
            itemBuilder: (context) => [
              PopupMenuItem(
                value: "Log Out",
                child: Text("Log Out"),
              ),
            ],
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(height: 10),
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(2),
              ),
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              color: Colors.white,
              elevation: 2,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 25),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          child: Image.asset(
                              Img.get('icons8_plumbing_48px.png'),
                              color: MyColors.primary),
                          width: 50,
                          height: 50,
                        ),
                        Container(width: 15),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("Ghana Water Company",
                                style: MyText.title(context)
                                    .copyWith(color: MyColors.grey_80)),
                            Container(height: 2),
                            Text("@Team_DigiMeter ",
                                style: MyText.caption(context)
                                    .copyWith(color: MyColors.grey_40))
                          ],
                        ),
                        Spacer(),
                      ],
                    ),
                    Container(height: 20),
                    Row(
                      children: <Widget>[
                        Container(
                            child: Icon(Icons.info, color: MyColors.grey_40),
                            width: 50),
                        Container(width: 15),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("Version",
                                style: MyText.subhead(context).copyWith(
                                    color: MyColors.grey_60,
                                    fontWeight: FontWeight.w500)),
                            Container(height: 2),
                            Text("1.0.0",
                                style: MyText.caption(context)
                                    .copyWith(color: MyColors.grey_40))
                          ],
                        ),
                        Spacer(),
                      ],
                    ),
                    Container(height: 20),
                    Row(
                      children: <Widget>[
                        Container(
                            child: Icon(Icons.sync, color: MyColors.grey_40),
                            width: 50),
                        Container(width: 15),
                        Text("Changelog",
                            style: MyText.subhead(context)
                                .copyWith(color: MyColors.grey_60)),
                        Spacer(),
                      ],
                    ),
                    Container(height: 20),
                    Row(
                      children: <Widget>[
                        Container(
                            child: Icon(Icons.book, color: MyColors.grey_40),
                            width: 50),
                        Container(width: 15),
                        Text("License",
                            style: MyText.subhead(context)
                                .copyWith(color: MyColors.grey_60)),
                        Spacer(),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(2),
              ),
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              color: Colors.white,
              elevation: 2,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.35,
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 25),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(Icons.wallpaper, color: Colors.blueGrey),
                        Container(
                            width: MediaQuery.of(context).size.width * 0.3),
                        Text("Authors",
                            style: MyText.subhead(context).copyWith(
                                color: MyColors.grey_80,
                                fontWeight: FontWeight.w500))
                      ],
                    ),
                    Container(height: 20),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: [
                            Icon(Icons.person, color: MyColors.grey_40),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.15,
                            ),
                            Text(
                              "Michael Akandi",
                              style: MyText.subhead(context).copyWith(
                                  color: MyColors.grey_60,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                        Container(height: 14),
                        Row(
                          children: [
                            Icon(Icons.person, color: MyColors.grey_40),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.15,
                            ),
                            Text(
                              "Abdul,Bashiru",
                              style: MyText.subhead(context).copyWith(
                                  color: MyColors.grey_60,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                        Container(height: 14),
                        Row(
                          children: [
                            Icon(Icons.person, color: MyColors.grey_40),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.15,
                            ),
                            Text(
                              "Confidence Antwi",
                              style: MyText.subhead(context).copyWith(
                                  color: MyColors.grey_60,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(2),
              ),
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              color: Colors.white,
              elevation: 2,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 25),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(width: 6),
                        Text("Company",
                            style: MyText.subhead(context).copyWith(
                                color: MyColors.grey_80,
                                fontWeight: FontWeight.w500))
                      ],
                    ),
                    Container(height: 20),
                    Row(
                      children: <Widget>[
                        Container(
                            child:
                                Icon(Icons.business, color: MyColors.grey_40),
                            width: 50),
                        Container(width: 15),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("Dream Space Inc.",
                                style: MyText.subhead(context).copyWith(
                                    color: MyColors.grey_60,
                                    fontWeight: FontWeight.w500)),
                            Container(height: 2),
                            Text("Android App Specialist",
                                style: MyText.caption(context)
                                    .copyWith(color: MyColors.grey_40))
                          ],
                        ),
                        Spacer(),
                      ],
                    ),
                    Container(height: 20),
                    Row(
                      children: <Widget>[
                        Container(
                            child: Icon(Icons.location_on,
                                color: MyColors.grey_40),
                            width: 50),
                        Container(width: 15),
                        Expanded(
                          child: Text(
                            "3265  Hinkle Deegan Lake Road, Dundee New York, United State",
                            style: MyText.subhead(context).copyWith(
                                color: MyColors.grey_60,
                                fontWeight: FontWeight.w500),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Container(height: 10),
          ],
        ),
      ),
    );
  }
}
