import 'dart:convert';
import 'dart:io';

import 'package:back_pressed/back_pressed.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:gwc/Model/Meter.dart';
import 'package:gwc/Model/Objects.dart';
import 'package:gwc/Model/cutom_dialog.dart';
import 'package:gwc/Model/paymentFile.dart';
import 'package:gwc/Model/urls.dart';
import 'package:gwc/Pages/EditUser.dart';
import 'package:gwc/Pages/MeterView.dart';
import 'package:gwc/Pages/my_custom_widgets.dart';
import 'package:gwc/Pages/settings.dart';
import 'package:gwc/helpers/my_textStyles.dart';
import 'package:gwc/helpers/my_colors.dart';
import 'package:paystack_manager/paystack_pay_manager.dart';
import 'package:shimmer/shimmer.dart';
import 'UserInfo.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

part 'floatingBottomsheet.dart';

class DashBoard extends StatefulWidget {
  final String value;
  DashBoard(this.value);

  @override
  DashBoardState createState() => new DashBoardState();
}

class DashBoardState extends State<DashBoard> {
  @override
  void initState() {
    super.initState();

    //  SQLiteDb dbHelper = SQLiteDb();
    //dbHelper.init();
    payment = Payment(context: context);
  }

  var data;
  List<MeterObject> meterList = [];
  bool showSheet = false;
  Payment payment;
  List<String> meterNameList = [];
  List<String> meterIdList = [];
  MyObjects objects = Get.find();
  PersistentBottomSheetController sheetController;
  BuildContext _scaffoldCtx;

  final storage = FlutterSecureStorage();
  final snackBar = SnackBar(
    content: const Text('Press again to exit'),
  );
  static const snackBarDuration = Duration(seconds: 3);
  DateTime backButtonPressTime;

  final _shimmerGradient = LinearGradient(
    colors: [
      Color(0xFFEBEBF4),
      Color(0xFFF4F4F4),
      Color(0xFFEBEBF4),
    ],
    stops: [
      0.1,
      0.3,
      0.4,
    ],
    begin: Alignment(-1.0, -0.3),
    end: Alignment(1.0, 0.3),
    tileMode: TileMode.clamp,
  );

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        final now = DateTime.now();
        final backButtonHasNotBeenPressedOrSnackBarHasBeenClosed =
            backButtonPressTime == null ||
                now.difference(backButtonPressTime) > snackBarDuration;

        if (backButtonHasNotBeenPressedOrSnackBarHasBeenClosed) {
          backButtonPressTime = now;
          //Scaffold.of(context).showSnackBar(snackBar);
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
          return false;
        }
        SystemNavigator.pop();
        return true;
      },
      child: new Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 1,
          backgroundColor: Colors.white,
          title: InkWell(
            onTap: () async {
              print("hi from inkwell");
            },
            child: Center(
              child: Row(
                children: [
                  Text(
                    "Welcome",
                    style: MyText.title(context).copyWith(
                      color: Colors.blue,
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.1,
                  ),
                  Text(
                    widget.value.toUpperCase(),
                    style: MyText.title(context).copyWith(
                      color: Colors.green,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 100,
                ),

//            Text("Pay Your Bills",
//                style: MyText.medium(context).copyWith(
//                    color: MyColors.grey_90, fontWeight: FontWeight.bold)),
//            Container(height: 10),
                Row(
                  children: <Widget>[
                    Container(width: 15),
                    Expanded(
                      child: InkWell(
                        splashColor: Colors.transparent,
                        focusColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () async {
                          await payment.getMeterInfo(context: context);
                          if (payment.data == null) {
                            objects.customErrorAlert(
                                context: context,
                                title: "No meter added",
                                content: "Please add a meter");
                          } else if (payment.data != null) {
                            showDialog(
                                context: context,
                                builder: (context) {
                                  return payment.showMyDialog();
                                });
                          }
                        },
                        child: Card(
                          elevation: 5.0,
                          child: Container(
                            padding: EdgeInsets.symmetric(vertical: 20),
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.money, size: 35, color: Colors.blue),
                                Container(height: 18),
                                Text("TOP UP",
                                    style: MyText.body1(context)
                                        .copyWith(color: MyColors.grey_90)),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(width: 6),
                    Expanded(
                      child: InkWell(
                        splashColor: Colors.transparent,
                        focusColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () {
                          Helper._showSheet(context);
                        },
                        child: Card(
                          elevation: 5.0,
                          child: Container(
                            padding: EdgeInsets.symmetric(vertical: 20),
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.contact_phone,
                                    size: 35, color: Colors.blue),
                                Container(height: 18),
                                Text("SUPPORT",
                                    style: MyText.body1(context)
                                        .copyWith(color: MyColors.grey_90)),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(width: 15),
                  ],
                ),
                Container(height: 6),
                Row(
                  children: <Widget>[
                    Container(width: 6),
                    Expanded(
                      child: InkWell(
                        splashColor: Colors.transparent,
                        focusColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => MyMeter()),
                          );
                        },
                        child: Card(
                          elevation: 5.0,
                          child: Container(
                            padding: EdgeInsets.symmetric(vertical: 20),
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.invert_colors,
                                    size: 35, color: Colors.blue),
                                Container(height: 18),
                                Text("METER ",
                                    style: MyText.body1(context)
                                        .copyWith(color: MyColors.grey_90)),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(width: 6),
                    Expanded(
                      child: InkWell(
                        splashColor: Colors.transparent,
                        focusColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SettingsPage()),
                          );
                        },
                        child: Card(
                          elevation: 5.0,
                          child: Container(
                            padding: EdgeInsets.symmetric(vertical: 20),
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.settings,
                                    size: 35, color: Colors.blue),
                                Container(height: 18),
                                Text("SETTINGS",
                                    style: MyText.body1(context)
                                        .copyWith(color: MyColors.grey_90)),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(width: 15),
                  ],
                ),
                Container(height: 30),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
