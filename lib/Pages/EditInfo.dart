import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:gwc/Model/Objects.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:gwc/Model/paymentFile.dart';
import 'package:gwc/Model/urls.dart';
import 'package:gwc/Pages/DashBoard.dart';
import 'package:gwc/Pages/LogIn.dart';
import 'package:gwc/Pages/UserInfo.dart';
import 'package:gwc/Pages/my_custom_widgets.dart';
import 'package:gwc/helpers/images.dart';
import 'package:gwc/helpers/my_colors.dart';
import 'package:gwc/helpers/my_textStyles.dart';
import 'package:http/http.dart' as http;
import 'package:paystack_manager/paystack_pay_manager.dart';

class EditInfo extends StatefulWidget {
  final String currentLock;
  MyObjects obj = Get.find();
  final meterIndex;
  final details;
  final storage = FlutterSecureStorage();
  final TextEditingController _name = TextEditingController();
  final TextEditingController _id = TextEditingController();
  final TextEditingController _uniqueName = TextEditingController();
  final TextEditingController _digitalAddress = TextEditingController();

  var name;
  var id;
  var uniqueName;
  var addr;
//for meter editor

  //MyMeter meterViewObj = Get.put(MyMeter(), permanent: true);

  void addEditMeter(index) {
    obj.addEditMeter(
      meterName: _name.text,
      meterId: _id.text,
      uniqueName: _uniqueName.text,
      meterAddr: _digitalAddress.text,
      index: index,
    );
  }

  EditInfo(this.details, this.currentLock, this.meterIndex);

  @override
  EditInfoState createState() => new EditInfoState();
}

class EditInfoState extends State<EditInfo> {
  @override
  void initState() {
    super.initState();
    //  SQLiteDb dbHelper = SQLiteDb();
    //dbHelper.init();
    checkMeterState();
  }

  Payment payment;
  checkMeterState() {
    if (widget.currentLock == "UNLOCKED") {
      myLockState = "Lock";
    } else if (widget.currentLock == "LOCKED") {
      myLockState = "Unlock";
    } else {
      myLockState = "lock/Unlock";
    }
  }

  String myLockState = "";
  var reading;
  String meterLock;
  final storage = FlutterSecureStorage();
  var data;
  bool loading = true;
  bool empty = true;
  MyObjects objects = Get.find();
  Widget buildSpeedDial() {
    return SpeedDial(
      elevation: 2,
      animatedIcon: AnimatedIcons.menu_close,
      curve: Curves.linear,
      animationSpeed: 100,
      overlayColor: Colors.black,
      overlayOpacity: 0.2,
      backgroundColor: MyColors.primary,
      children: [
        SpeedDialChild(
          elevation: 2,
          label: "$myLockState", //Lock Meter
          child: Icon(Icons.lock, color: MyColors.grey_80),
          backgroundColor: Colors.white,
          onTap: () async {
            showLoading(context);
            String token = await storage.read(key: "jwt");
            var meterId = widget.details["meter_id"];
            var url = Uri.parse(
                "$baseUrl/digi_rest/api/handler2.php?meter_id=$meterId&lock=1");
            //  "http://localhost/digi_rest/api/handler2.php?meter_id=14357098432&get_readings=1");
            await http.get(
              url,
              headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
            ).then((response) async {
              print("lock ");
              print(jsonDecode(response.body));
              var me = jsonDecode(response.body);
              String firstName = await storage.read(key: "first_name");

              if (me["message"] == "Success") {
                objects.dashBoardNavDialog(
                    context: context,
                    firstName: firstName,
                    text: "Success",
                    content: "You will be redirected to your dashboard");
              } else if (me["data"] == "Expired token") {
                widget.obj.expiredTokenChecks(context);
              } else {
                widget.obj.errorAlert(context);
              }
            });
          },
        ),
        SpeedDialChild(
          elevation: 2,
          label: "Borrow Credit",
          child: Icon(Icons.account_balance, color: MyColors.grey_80),
          backgroundColor: Colors.white,
          onTap: () async {
            await borrowDialog();
          },
        ),
        SpeedDialChild(
          elevation: 2,
          label: "Top Up",
          child: Icon(Icons.credit_card, color: MyColors.grey_80),
          backgroundColor: Colors.white,
          onTap: () async {
            //_checkPayment();
            // widget.obj.showMyDialog(context);
            // await payment.getMeterInfo(context: context);
            showDialog(
                context: context,
                builder: (context) {
                  return payment
                      .showSpecificMeterDialog(widget.details["meter_id"]);
                });
          },
        ),
        SpeedDialChild(
          elevation: 2,
          label: "Details",
          child: Icon(Icons.menu, color: MyColors.grey_80),
          backgroundColor: Colors.white,
          onTap: () async {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => UserInfo(
                      details: widget.details, index: widget.meterIndex)),
            );
          },
        ),
      ],
    );
  }

  borrowCredit() async {
    showLoading(context);
    String token = await storage.read(key: "jwt");
    var meterId = widget.details["meter_id"];
    var my_url = Uri.parse(
        "$baseUrl/digi_rest/api/handler2.php?meter_id=$meterId&borrow=1");
    await http.get(
      my_url,
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
    ).then((response) async {
      var me = jsonDecode(response.body);
      String firstName = await storage.read(key: "first_name");
      print(" Borrowing");
      print(me);
      if (me["message"] == "Success") {
        objects.dashBoardNavDialog(
            context: context,
            firstName: firstName,
            text: "Success",
            content: "You will be redirected to your dashboard");
      } else if (me["data"] == "Expired token") {
        widget.obj.expiredTokenChecks(context);
      } else if (me["message"] == "You can't borrow") {
        //Navigator.pop(context);
        widget.obj.customErrorAlert(
            context: context,
            title: "Operation Failed",
            content: me["message"]);
        // objects.dashBoardNavDialog(
        //     context: context,
        //     firstName: firstName,
        //     text: "Operation Failed",
        //     content: me["message"]);
      } else {
        widget.obj.errorAlert(context);
      }
    });
  }

  borrowDialog() {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Borrow Credit'),
          content: SingleChildScrollView(
            child: Text('Do you Confirm ?'),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Cancel'),
              onPressed: () async {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Confirm'),
              onPressed: () {
                Navigator.of(context).pop();
                showLoading(context);
                borrowCredit();
              },
            ),
          ],
        );
      },
    );
  }

  delMeterDialog() {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Meter Deletion'),
          content: SingleChildScrollView(
            child: Text('Do you Confirm meter deletion?'),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Cancel'),
              onPressed: () async {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Confirm'),
              onPressed: () {
                Navigator.of(context).pop();
                showLoading(context);
                deleteMeter();
              },
            ),
          ],
        );
      },
    );
  }

  deleteMeter() async {
    String id = await storage.read(key: "customer_id");
    String token = await storage.read(key: "jwt");
    var meterId = widget.details["meter_id"];
    var url = Uri.parse(
        "$baseUrl/digi_rest/api/handler.php?meter_id=$meterId&customer_id=$id&delete_alias=1");
    //  "http://localhost/digi_rest/api/handler2.php?meter_id=14357098432&get_readings=1");
    await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
    ).then((response) async {
      print(response.body);
      var me = jsonDecode(response.body);
      print("Hi I am here");
      print(me);
      String firstName = await storage.read(key: "first_name");
      if (me["message"] == "Success") {
        Navigator.pop(context);
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: widget.obj.dashBoardNavDialog(
                context: context,
                firstName: firstName,
                text: "Success",
                content: "You will be redirected to your dashboard"));
      } else if (me["data"] == "Expired token") {
        widget.obj.expiredTokenChecks(context);
      } else {
        Navigator.pop(context);
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Error"),
              content: Text("${me["message"]}"),
              actions: <Widget>[
                FlatButton(
                  child: const Text('OKAY'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          },
        );
      }
      setState(() {
        data = me["data"];
        loading = false;
      });

      if (data.length > 0) {
        setState(() {
          empty = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    bool isSwitched1 = true, isSwitched2 = true;
    bool isSwitched3 = true, isSwitched4 = true;
    bool isSwitched5 = false, isSwitched6 = false;
    payment = Payment(context: context);
    return new Scaffold(
      backgroundColor: Colors.white,
      floatingActionButton: buildSpeedDial(),
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.blue),
        elevation: 1,
        backgroundColor: Colors.white,
        title: Text("Edit Meter",
            style: MyText.title(context).copyWith(
              color: Colors.blue,
            )),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(height: 10),
            Container(
              child: Row(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("Customer ID",
                          style: MyText.headline(context).copyWith(
                              color: MyColors.grey_80,
                              fontWeight: FontWeight.bold)),
                      Text("${widget.details["customer_id"]}",
                          style: MyText.body1(context)
                              .copyWith(color: MyColors.grey_80)),
                    ],
                  ),
                  Spacer(),
                  IconButton(
                    icon: Icon(
                      Icons.delete,
                      size: 30,
                    ),
                    onPressed: () {
                      // deleteMeter();
                      delMeterDialog();
                    },
                    color: Colors.red,
                  )
                ],
              ),
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 25),
            ),
            InkWell(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (_) => CustomEventDialog(
                          change: '',
                          title: 'Name',
                          meterId: widget.details["meter_id"],
                        ));
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 25),
                child: Row(
                  children: <Widget>[
                    Text("Meter Name",
                        style: MyText.medium(context).copyWith(
                            color: MyColors.grey_80,
                            fontWeight: FontWeight.w300)),
                    Spacer(),
                    Text("${widget.details["meter_alias"]}",
                        textAlign: TextAlign.right,
                        style: MyText.medium(context).copyWith(
                            color: MyColors.grey_80,
                            fontWeight: FontWeight.w300)),
                    Container(width: 10)
                  ],
                ),
              ),
            ),
            Divider(height: 0),
            InkWell(
              onTap: () {
                // showDialog(context: context,builder: (_) => CustomEventDialog(change: '',  title: 'Id',) );
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 25),
                child: Row(
                  children: <Widget>[
                    Text("Meter ID",
                        style: MyText.medium(context).copyWith(
                            color: MyColors.grey_80,
                            fontWeight: FontWeight.w300)),
                    Spacer(),
                    Text("${widget.details["meter_id"]}",
                        textAlign: TextAlign.right,
                        style: MyText.medium(context).copyWith(
                            color: MyColors.grey_80,
                            fontWeight: FontWeight.w300)),
                    Container(width: 10)
                  ],
                ),
              ),
            ),
            Divider(height: 0),
            InkWell(
              onTap: () {
                // showDialog(context: context,builder: (_) => CustomEventDialog(change: '',  title: 'Unique Name',) );
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 25),
                child: Row(
                  children: <Widget>[
                    Text("Meter Account",
                        style: MyText.medium(context).copyWith(
                            color: MyColors.grey_80,
                            fontWeight: FontWeight.w300)),
                    Spacer(),
                    Text("GHS ${widget.details["meter_account"]}",
                        textAlign: TextAlign.right,
                        style: MyText.medium(context).copyWith(
                            color: MyColors.grey_80,
                            fontWeight: FontWeight.w300)),
                    Container(width: 10)
                  ],
                ),
              ),
            ),
            Divider(height: 0),
            InkWell(
              onTap: () {
                //showDialog(context: context,builder: (_) => CustomEventDialog(change:  '',  title: 'Address',) );
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 25),
                child: Row(
                  children: <Widget>[
                    Text("Lock Status",
                        style: MyText.medium(context).copyWith(
                            color: MyColors.grey_80,
                            fontWeight: FontWeight.w300)),
                    Spacer(),
                    Text("${widget.details["lock_status"]}",
                        textAlign: TextAlign.right,
                        style: MyText.medium(context).copyWith(
                            color: MyColors.grey_80,
                            fontWeight: FontWeight.w300)),
                    Container(width: 10)
                  ],
                ),
              ),
            ),
            Divider(height: 0),
            InkWell(
              onTap: () {
                //showDialog(context: context,builder: (_) => CustomEventDialog(change:  '',  title: 'Address',) );
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 25),
                child: Row(
                  children: <Widget>[
                    Text("Health Status",
                        style: MyText.medium(context).copyWith(
                            color: MyColors.grey_80,
                            fontWeight: FontWeight.w300)),
                    Spacer(),
                    Text("${widget.details["health_status"]}",
                        textAlign: TextAlign.right,
                        style: MyText.medium(context).copyWith(
                            color: MyColors.grey_80,
                            fontWeight: FontWeight.w300)),
                    Container(width: 10)
                  ],
                ),
              ),
            ),
            Divider(height: 0),
          ],
        ),
      ),
    );
  }

  void _checkPayment() {
    try {
      PaystackPayManager(context: context)
        ..setSecretKey("sk_test_cb859b103a9e82864c7dd83893960d234c144286")
        ..setCompanyAssetImage(
            Image(image: AssetImage('images/icons8_plumbing_48px.png')))
        ..setAmount(100000)
        ..setReference(DateTime.now().millisecondsSinceEpoch.toString())
        ..setCurrency("GHS")
        ..setEmail("famousmichael71@gmail.com")
        ..setFirstName("Michael")
        ..setLastName("Opoku")
        ..setMetadata({
          "custom_fields": [
            {
              "value": "TechWith Michael",
              "display_name": "Payment_to",
              "variable_name": "Payment_to"
            }
          ]
        })
        ..onSuccesful(_onPaymentSuccessful)
        ..onPending(_onPaymentPending)
        ..onFailed(_onPaymentFailed)
        ..onCancel(_onCancel)
        ..initialize();
    } catch (error) {
      print("Payment error --- $error");
    }
  }

  void _onPaymentSuccessful(Transaction transaction) {
    print('Success');
    print(
        "transaaction message --> ${transaction.message}, Ref ${transaction.refrenceNumber}");
  }

  void _onPaymentPending(Transaction transaction) {
    print('Pending');
    print(
        "transaaction message --> ${transaction.message}, Ref ${transaction.refrenceNumber}");
  }

  void _onPaymentFailed(Transaction transaction) {
    print('Failed');
    print(
        "transaaction message --> ${transaction.message}, Ref ${transaction.refrenceNumber}");
  }

  void _onCancel(Transaction transaction) {
    print('cancelled');
    print(
        "transaaction message --> ${transaction.message}, Ref ${transaction.refrenceNumber}");
  }
}

class CustomEventDialog extends StatefulWidget {
  final String change;
  final String title;
  final String meterId;

  CustomEventDialog({Key key, this.change, this.title, this.meterId})
      : super(key: key);

  @override
  CustomEventDialogState createState() => new CustomEventDialogState();
}

class CustomEventDialogState extends State<CustomEventDialog> {
  final storage = FlutterSecureStorage();
  var meterNameCont = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
            Material(
              color: MyColors.primary,
              child: Container(
                height: 50,
                child: Row(
                  children: <Widget>[
                    IconButton(
                      icon: const Icon(
                        Icons.close,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    Text("Edit Meter ${widget.title}",
                        style: TextStyle(color: Colors.white, fontSize: 20)),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(height: 25),
                  TextFormField(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      print(value);
                      if (value == null || value == " ") {
                        print("I am here");

                        return "Please enter meter name";
                      }
                      return null;
                    },
                    controller: meterNameCont,
                    style: TextStyle(fontSize: 18),
                    decoration: InputDecoration(
                      hintText: "Enter Meter Name", //"${widget.change}",
                      hintStyle: TextStyle(color: Colors.grey),
                    ),
                  ),
                  Container(height: 25),
                  Align(
                    alignment: Alignment.centerRight,
                    child: RaisedButton(
                      child: Text(
                        "SAVE",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.blue[700],
                      onPressed: () async {
                        showLoading(context);
                        String token = await storage.read(key: "jwt");
                        String firstName =
                            await storage.read(key: "first_name");
                        var url = "$baseUrl/digi_rest/api/handler.php";
                        var meterName = meterNameCont.text;
                        var customerId = await storage.read(key: "customer_id");
                        final my_header = {
                          HttpHeaders.authorizationHeader: "Bearer $token"
                        };

                        var json = {
                          "update_meter_alias": "1",
                          "meter_id": "${widget.meterId}",
                          "customer_id": "$customerId",
                          "meter_alias": "$meterName"
                        };
                        if (meterName != null && meterName != "") {
                          await http
                              .post(url,
                                  headers: my_header, body: jsonEncode(json))
                              .then((response) {
                            var me = jsonDecode(response.body);
                            if (me["message"] == "Success") {
                              Navigator.pop(context);
                              showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text("Success"),
                                    content: Text(
                                        "You will be redirected to your dashboard"),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: const Text('OKAY'),
                                        onPressed: () {
                                          Navigator.pushAndRemoveUntil(
                                            context,
                                            MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  DashBoard(firstName),
                                            ),
                                            (route) => false,
                                          );
                                        },
                                      )
                                    ],
                                  );
                                },
                              );
                            } else {
                              Navigator.pop(context);
                              showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text("Error"),
                                    content: Text("${me["message"]}"),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: const Text('OKAY'),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      )
                                    ],
                                  );
                                },
                              );
                            }
                          });
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
            Container(height: 20)
          ],
        ),
      ),
    );
  }
}

class CircleImage extends StatelessWidget {
  final double size;
  final Color backgroundColor;
  final ImageProvider imageProvider;

  const CircleImage({
    Key key,
    @required this.imageProvider,
    this.size,
    this.backgroundColor,
  })  : assert(imageProvider != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasMediaQuery(context));
    return Container(
        width: size != null ? size : 20,
        height: size != null ? size : 20,
        decoration: new BoxDecoration(
            shape: BoxShape.circle,
            color:
                backgroundColor != null ? backgroundColor : Colors.transparent,
            image:
                new DecorationImage(fit: BoxFit.fill, image: imageProvider)));
  }
}
