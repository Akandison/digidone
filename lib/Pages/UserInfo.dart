import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:back_pressed/back_pressed.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:gwc/Model/Objects.dart';
import 'package:gwc/Model/urls.dart';
import 'package:gwc/Pages/Graph.dart';
import 'package:gwc/Pages/my_custom_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:gwc/helpers/images.dart';
import 'package:gwc/helpers/my_colors.dart';
//import 'package:materialx_flutter/model/folder_file.dart';
import 'package:gwc/helpers/my_textStyles.dart';

class UserInfo extends StatefulWidget {
  final details;
  final index;
  UserInfo({this.details, this.index});

  @override
  UserInfoState createState() => new UserInfoState();
}

class UserInfoState extends State<UserInfo> {
  @override
  void dispose() {
    // TODO: implement dispose
    canSetState = false;
    timer.cancel();

    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    //  SQLiteDb dbHelper = SQLiteDb();
    //dbHelper.init();
    allInOne();
    setUpTimedFetch();
  }

  //

  Urls urlObjects = Urls();
  MyObjects objects = Get.find();
  final timeNow = TimeOfDay.now();
  var loading = true;
  var empty = true;
  var storage = FlutterSecureStorage();
  var data;
  var balance;
  var reading;
  var isReadingLoading = true;
  var isBalanceLoading = true;
  var isPaymentLoading = true;
  var isPaymentEmpty = true;
  var readList = [];
  var timeList = [];
  var responseLength = 10;
  List entriesList;
  var entriesMap = Map();
  var dataToMap;
  var timer;
  var canSetState = true;
  var payments;
  List paymentsList = [];
  String currency = "GHS";
  MyObjects objectsCont = Get.find();
  setUpTimedFetch() {
    timer = Timer.periodic(Duration(milliseconds: 5000), (timer) {
      balanceChecker();
      readingChecker();
      getPaymentHistory();
    });
  }

  getPaymentHistory() async {
    String token = await storage.read(key: "jwt");
    var meterId = widget.details["meter_id"];
    String id = await storage.read(key: "customer_id");
    //var firstName = await storage.read(key: "first_name");

    var url = Uri.parse(
        "$baseUrl/digi_rest/api/payment_handler.php?customer_id=$id&meter_id=$meterId&get_payment=1");
    http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
    ).then((response) async {
      var me = jsonDecode(response.body);
      print("Hello from payments");
      print(me["data"]);
      if (me["data"] != null) {
        if (canSetState) {
          setState(() {
            paymentsList = me["data"];
            isPaymentLoading = false;
            isPaymentEmpty = false;
          });
        }
      }
    });
  }

  getDataMap() {
    // entriesList.clear();
    readList.clear();
    timeList.clear();
    if (entriesList != null) {
      for (int i = 0; i < responseLength; i++) {
        print("where I want");
        String readingNow = entriesList[i]["volume_consumed"].toString();
        print("ent");
        print(readingNow);
        readList.insert(i, readingNow);
        String now = entriesList[i]["entry_time"];

        //var myTy = entriesList[i]["entry_time"].toString();
        timeList.insert(i, now);
      }
      print("read list");
      print(readList);
      //entriesMap["readings"] = readList;
      //entriesMap["time"] = timeList;
      print("Reading and time");
      print(readList);
      print(timeList);
    }
  }

  balanceChecker() async {
    String id = await storage.read(key: "customer_id");
    String token = await storage.read(key: "jwt");

    var url = Uri.parse(
        "$baseUrl/digi_rest/api/handler.php?customer_id=$id&alias=1&no=0");
    http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
    ).then((response) {
      var me = jsonDecode(response.body);
      if (me["data"] == "Expired token") {
        objectsCont.expiredTokenChecks(context);
      }
      if (canSetState) {
        setState(() {
          balance = me["data"][widget.index]["meter_account"];
          isBalanceLoading = false;
        });
      }

      // readingChecker();
    });
  }

  readingChecker() async {
    // showLoading(context);
    //String id = await storage.read(key: "customer_id");
    String token = await storage.read(key: "jwt");
    var meterId = widget.details["meter_id"];
    //var firstName = await storage.read(key: "first_name");

    var url = Uri.parse(
        "$baseUrl/digi_rest/api/handler2.php?meter_id=$meterId&get_readings=1");
    http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
    ).then((response) async {
      var me = jsonDecode(response.body);
      entriesList = me["data"];
      print("I am entry");
      print(entriesList);
      //reading = me["data"][0]["reading"];
      getDataMap();
      if (me["data"] != null) {
        if (canSetState) {
          setState(() {
            reading = me["data"][0]["reading"];
            isReadingLoading = false;
          });
        }
      } else {
        if (canSetState) {
          setState(() {
            reading = 0;
            isReadingLoading = false;
          });
        }
      }
    });
  }

  checkMeters() async {
    String id = await storage.read(key: "customer_id");
    String token = await storage.read(key: "jwt");

    var url = Uri.parse(
        "$baseUrl/digi_rest/api/handler.php?customer_id=$id&alias=1&no=0");
    http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
    ).then((response) {
      print(response.body);
      var me = jsonDecode(response.body);
      print("Payment History");
      print(me["data"]);
      //currentLock = me["data"][0]["lock_status"];
      if (me["data"] == "Expired token") {
        objectsCont.expiredTokenChecks(context);
      }

      setState(() {
        data = me["data"];
        loading = false;
      });

      if (data.length > 0) {
        setState(() {
          empty = false;
        });
      }
    });
  }

  allInOne() async {
    await readingChecker();
    await balanceChecker();
    await getPaymentHistory();
  }

  borrowDialog() {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Borrow Credit'),
          content: SingleChildScrollView(
            child: Text('Do you Confirm ?'),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Cancel'),
              onPressed: () async {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Confirm'),
              onPressed: () {
                Navigator.of(context).pop();
                showLoading(context);
                borrowCredit();
              },
            ),
          ],
        );
      },
    );
  }

  void borrowCredit() async {
    showLoading(context);
    String token = await storage.read(key: "jwt");
    var meterId = widget.details["meter_id"];
    var my_url = Uri.parse(
        "$baseUrl/digi_rest/api/handler2.php?meter_id=$meterId&borrow=1");
    await http.get(
      my_url,
      headers: {HttpHeaders.authorizationHeader: "Bearer $token"},
    ).then((response) async {
      var me = jsonDecode(response.body);
      String firstName = await storage.read(key: "first_name");
      print(" Borrowing");
      print(me);
      if (me["message"] == "Success") {
        objects.dashBoardNavDialog(
            context: context,
            firstName: firstName,
            text: "Success",
            content: "You will be redirected to your dashboard");
      } else if (me["data"] == "Expired token") {
        objects.expiredTokenChecks(context);
      } else if (me["message"] == "You can't borrow") {
        //Navigator.pop(context);
        objects.customErrorAlert(
            context: context,
            title: "Operation Failed",
            content: me["message"]);
        // objects.dashBoardNavDialog(
        //     context: context,
        //     firstName: firstName,
        //     text: "Operation Failed",
        //     content: me["message"]);
      } else {
        objects.errorAlert(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // print("isbalance");
    // print(isBalanceLoading);
    // print("isreading");
    // print(isReadingLoading);

    return OnBackPressed(
      perform: () {
        print("Back Pressed");
        timer.cancel();
        Navigator.pop(context);
      },
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          backgroundColor: Colors.white,
          brightness: Brightness.dark,
          iconTheme: IconThemeData(color: Colors.blue),
          title: Text("User Data",
              style: MyText.title(context).copyWith(color: Colors.blue)),
        ),
        body: isBalanceLoading == true ||
                isReadingLoading == true ||
                isPaymentLoading == true
            ? ListView.builder(
                shrinkWrap: true,
                itemCount: 3,
                itemBuilder: (BuildContext context, int index) {
                  return LoadingMeters();
                })
            :

            //If after running checkMeter, we found out that user has no meter.
            // isReadingLoading == true
            //     ? ListView.builder(
            //         shrinkWrap: true,
            //         itemCount: 3,
            //         itemBuilder: (BuildContext context, int index) {
            //           return LoadingMeters();
            //         })
            //     :
            SingleChildScrollView(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Card(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2)),
                            color: Colors.white,
                            elevation: 2,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 15, horizontal: 15),
                              child: Row(
                                children: <Widget>[
                                  CircleAvatar(
                                    backgroundColor: Colors.lightGreen[500],
                                    child: Icon(
                                      Icons.file_download,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Container(width: 10),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        width: 75,
                                        child: SingleChildScrollView(
                                          scrollDirection: Axis.horizontal,
                                          child: Text(
                                            'Total Flow ',
                                            style: MyText.subhead(context)
                                                .copyWith(
                                                    color: MyColors.grey_60,
                                                    fontWeight:
                                                        FontWeight.bold),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                      Container(height: 5),
                                      reading != null
                                          ? Text(
                                              "$reading" + "L",
                                              style: MyText.caption(context)
                                                  .copyWith(
                                                      color: MyColors.grey_40),
                                              textAlign: TextAlign.center,
                                            )
                                          : Text(
                                              "",
                                              style: MyText.caption(context)
                                                  .copyWith(
                                                      color: MyColors.grey_40),
                                              textAlign: TextAlign.center,
                                            )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(width: 5),
                        Expanded(
                          child: Card(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2)),
                            color: Colors.white,
                            elevation: 2,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 15, horizontal: 15),
                              child: Row(
                                children: <Widget>[
                                  CircleAvatar(
                                    backgroundColor: Colors.indigo[400],
                                    child: Icon(
                                      Icons.money,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Container(width: 10),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        'Bal',
                                        style: MyText.subhead(context).copyWith(
                                            color: MyColors.grey_60,
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.center,
                                      ),
                                      Container(height: 5),
                                      balance != null
                                          ? Text(
                                              balance,
                                              style: MyText.caption(context)
                                                  .copyWith(
                                                      color: MyColors.grey_40),
                                              textAlign: TextAlign.center,
                                            )
                                          : Text(
                                              "",
                                              style: MyText.caption(context)
                                                  .copyWith(
                                                      color: MyColors.grey_40),
                                              textAlign: TextAlign.center,
                                            )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(height: 5),

                    //Shimmer will work here

                    // Row(
                    //   children: <Widget>[
                    //     Expanded(
                    //       child: Card(
                    //         shape: RoundedRectangleBorder(
                    //             borderRadius: BorderRadius.circular(2)),
                    //         color: Colors.white,
                    //         elevation: 2,
                    //         clipBehavior: Clip.antiAliasWithSaveLayer,
                    //         child: Container(
                    //           padding: EdgeInsets.symmetric(
                    //               vertical: 15, horizontal: 15),
                    //           child: Row(
                    //             children: <Widget>[
                    //               CircleAvatar(
                    //                 backgroundColor: Colors.lightGreen[500],
                    //                 child: Icon(
                    //                   Icons.description,
                    //                   color: Colors.white,
                    //                 ),
                    //               ),
                    //               Container(width: 10),
                    //               Text(
                    //                 "Payments Statements",
                    //                 style: MyText.subhead(context).copyWith(
                    //                     color: MyColors.grey_60,
                    //                     fontWeight: FontWeight.bold),
                    //                 textAlign: TextAlign.center,
                    //               )
                    //             ],
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //   ],
                    // ),
                    // Container(height: 5),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.6,
                      child: isPaymentEmpty
                          ? Container(
                              child: Center(child: Text("No Payment yet")),
                            )
                          : ListView.builder(
                              itemCount: paymentsList.length,
                              itemBuilder: (context, index) {
                                return Card(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(2)),
                                  color: Colors.white,
                                  elevation: 2,
                                  clipBehavior: Clip.antiAliasWithSaveLayer,
                                  child: Container(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 20, horizontal: 20),
                                          child: Row(
                                            children: <Widget>[
                                              Text(
                                                "Amount: $currency${paymentsList[index]["amount"]}",
                                                style: MyText.subhead(context)
                                                    .copyWith(
                                                        color: MyColors.grey_60,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                textAlign: TextAlign.center,
                                              ),
                                            ],
                                          ),
                                        ),
                                        Divider(height: 0),
                                        Container(height: 15),
                                        Container(
                                          padding: EdgeInsets.symmetric(
                                              vertical: 10, horizontal: 20),
                                          child: Row(
                                            children: <Widget>[
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "${paymentsList[index]["entry_time"]}",
                                                    style: MyText.body2(context)
                                                        .copyWith(
                                                            color: MyColors
                                                                .grey_60),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  SizedBox(
                                                    height: 20,
                                                  ),
                                                  Text(
                                                    "Transaction Id : ${paymentsList[index]["transaction_id"]}",
                                                    style: MyText.body2(context)
                                                        .copyWith(
                                                            color: MyColors
                                                                .grey_40,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ],
                                              ),
                                              Spacer(),
                                              // SizedBox(width: ),
                                              Text(
                                                "Top Up",
                                                style: MyText.body2(context)
                                                    .copyWith(
                                                        color: MyColors.grey_40,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                textAlign: TextAlign.center,
                                              ),
                                            ],
                                          ),
                                        ),
                                        Divider(),
                                      ],
                                    ),
                                  ),
                                );
                              }),
                    ),
                    Container(height: 5),
                    Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(2)),
                      color: Colors.white,
                      elevation: 2,
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            InkWell(
                              splashColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              onTap: () async {
                                await borrowDialog();
                              },
                              child: Container(
                                child: Row(
                                  children: [
                                    CircleAvatar(
                                      backgroundColor: Colors.blue[500],
                                      child: Icon(
                                        Icons.account_balance_wallet,
                                        color: Colors.white,
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    Text("Borrow Credit"),
                                  ],
                                ),
                                height: 70,
                              ),
                            ),
                            InkWell(
                              splashColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              onTap: () {
                                // getDataMap();
                                // timer.cancel();
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => MyGraph(
                                        details: widget.details,
                                        timeList: timeList,
                                        readList: readList,
                                        index: widget.index),
                                  ),
                                );
                              },
                              child: Container(
                                child: Row(
                                  children: [
                                    CircleAvatar(
                                      backgroundColor: Colors.blue[500],
                                      child: Icon(
                                        Icons.bar_chart_sharp,
                                        color: Colors.white,
                                      ),
                                    ),
                                    SizedBox(width: 10),
                                    Text("Usage"),
                                  ],
                                ),
                                height: 70,
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }
}
